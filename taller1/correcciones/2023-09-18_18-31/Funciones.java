package aed;

class Funciones {
    int cuadrado(int x) {
        return x*x;
    }

    double distancia(double x, double y) {
        return Math.sqrt(x*x+y*y);
    }

    boolean esPar(int n) {
        return n % 2 == 0;
    }

    boolean esBisiesto(int n) {
        return (n % 4 == 0 & ! (n % 100 == 0)) | n % 400 == 0;
    }

    int factorialIterativo(int n) {
        int factorial = 1;
        while (n!=0) {
            factorial *= n;
            n -=1;
        }
        return factorial;
    }

    int factorialRecursivo(int n) {
        if (n==0) {
            return 1;
        } else {
            return n * factorialRecursivo(n-1);
        }
    }

    boolean esPrimo(int n) {
        boolean primo = true;
        if (n < 2) {
            primo = false;
        } else {
            int i = 2;
            while (i <= Math.sqrt(Double.valueOf(n)) & primo) {
            if (n%i==0) {
                primo = false;
            }
            i++;
            }
        }
        return primo;
    }

    int sumatoria(int[] numeros) {
        int suma = 0;
        for (int i=0; i < numeros.length ; i++) {
            suma += numeros[i];
        }
        return suma;
    }

    int busqueda(int[] numeros, int buscado) {
        for (int i=0; i < numeros.length ; i++) {
            if (numeros[i] == buscado) {
                return i;
            }
        }
        return 0;
    }

    boolean tienePrimo(int[] numeros) {
        boolean tienePrimo = false;
        int i = 0;
        while (i < numeros.length & !tienePrimo) {
            if (esPrimo(numeros[i])) {
                tienePrimo = true;
            }
            i++;
        }
        return tienePrimo;
    }

    boolean todosPares(int[] numeros) {
        boolean todosPares = true;
        int i = 0;
        while (i < numeros.length & todosPares) {
            if (numeros[i] % 2 != 0) {
                todosPares = false;
            }
            i++;
        }
        return todosPares;
    }

    boolean esPrefijo(String s1, String s2) {
        boolean prefijo = true;
        if (s1.length() > s2.length()) {
            prefijo = false;
        } else {
            int i = 0;
            while (i < s1.length() & prefijo) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    prefijo = false;
                }
                i++;
            }
        }
        return prefijo;
    }

    boolean esSufijo(String s1, String s2) {
        int posicion = s2.length() - s1.length();
        boolean sufijo;
        if (posicion < 0) {
            sufijo = false;
        } else {
            // en lugar de equals, podria hacerse con esSufijo -como dice la consigna.
            sufijo = s1.equals(s2.substring(posicion));
        }
        return sufijo;
    }
}
