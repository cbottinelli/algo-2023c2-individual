package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] vector = new float[largo];

        for (int i = 0; i < largo; i++) {
            vector[i] = entrada.nextFloat();
        }

        return vector;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] matriz = new float[filas][columnas];

        for (int i = 0; i < filas; i++) {
                matriz[i] = leerVector(entrada, columnas);
            }
        
        return matriz;

        }

    void imprimirPiramide(PrintStream salida, int alto) {
        
        int largoDeLinea = 2*alto-1;

        // Cada iteracion genera una línea
        for (int linea = 0; linea < alto; linea++) {
            
            int espacios = alto-(linea+1);
            int estrellas = largoDeLinea-(2*espacios);

            // Cada iteración imprime un caracter
            for (int caracter = 0; caracter < largoDeLinea; caracter++) {
                if (caracter < espacios || caracter >= espacios + estrellas ) {
                    salida.print(" ");
                } else {
                    salida.print("*");
                }
            }

            salida.println();

        }
    }
}
