package aed;
public class Heapsort<T extends Comparable<T>> {
    
    T[] elems;
    int indice;


    //Construye un max-heap a partir de un array usando el algoritmo de Floyd.
    Heapsort(T[] arreglo) {
        
        elems = (T[]) arreglo.clone();
        indice = arreglo.length;

        // actual = ultimo nodo con hijos
        int actual = (indice-2)/2;

        //Algoritmo de Floyd,


        while(actual>=0){
            bajar(actual);
            actual--;
        }
    }

    public boolean vacia() {
        //complejidad = O(1)
        return indice == 0;
    }

    public boolean llena() {
        //complejidad = O(1)
        return indice == elems.length;
    }

    public void encolar(T elem) { // throws Exception {
        /* complejidad: O(log(indice))
         * Sea n la cantidad de nodos de un arbol y k = floor(log2(n)) + 1 la altura de un arbol,
         * en el peor caso en que elem > elems[0], el while(L90) se ejecutara k-1 veces = floor(log2(n)). 
         */
        /* if (llena()) {
            throw new Exception("Cola de Prioridad Llena.");
        } else { */
            elems[indice] = elem;
            int actual = indice;
            indice++;
            while(actual !=0 && elems[actual].compareTo(elems[(actual - 1)/2]) > 0) {
                swap((actual - 1)/2, actual);
                actual = (actual - 1)/2;
            }
        // }
        
    }

    public T desencolar() { //throws Exception {
        /* complejidad: O(log(indice))
         * La operacion mas costosa es L107, con complejidad O(log(indice))
         */
        //if (vacia()) {
            //throw new Exception("Cola de Prioridad Vacia.");
        //} else {
            T elemento = elems[0];
            elems[0] = elems[indice-1];
            bajar(0);
            indice--;
            return elemento;
       // }
    }

    //Devuelve el máximo, sin desencolarlo.
    public T maximo() {
        //complejidad = O(1)
        return elems[0];
    }

// METODOS AUXILIARES ///////////////////////////////////////////////////////////////////////////////////

    private void swap(int nodo1, int nodo2) {
        //requiere: nodo 1 y nodo 2 <= indice
        //complejidad: O(1)
        T temp = elems[nodo1];
        elems[nodo1] = elems[nodo2];
        elems[nodo2] = temp;
    }

    private void bajar(int nodo) {
        //requiere: 0 <= nodo < indice
        /*complejidad: O(log(indice))
         * Sea n la cantidad de nodos de un arbol y k = floor(log2(n)) + 1 la altura de un arbol,
         * en el peor caso en que nodo = 0, el while(L134) se ejecutara k-1 veces = floor(log2(n)).
         */
        while(existeHijoMayor(nodo)) {
                
                //caso 1: nodo tiene unicamente un hijo izquierdo.
                if (indice-1 == nodo*2 + 1) {
                    swap(nodo, nodo*2 + 1);
                    nodo = nodo*2 + 1;

                //caso 2: nodo tiene dos hijos.    
                } else if (indice-1 >= nodo*2 + 2) {
                    int hijoMayor = hijoMayor(nodo);
                    swap(nodo, hijoMayor);
                    nodo = hijoMayor;
                }
                
            }
    }

    private int hijoMayor(int actual) {
        //complejidad: O(1)
        //requiere: actual tiene dos hijos.
        int hijoMayor;
        if (elems[actual*2+1].compareTo(elems[actual*2+2]) > 0) {
            hijoMayor = actual*2+1;
        } else {
            hijoMayor = actual*2+2;
        }
        return hijoMayor;
    }

    private boolean existeHijoMayor(int actual) {
        //complejidad: O(1)
        return  (indice-1 == actual*2 + 1 && elems[actual].compareTo(elems[actual*2 + 1]) < 0) ||
                (indice-1 >= actual*2 + 2 && (elems[actual].compareTo(elems[actual*2 + 1]) < 0 || elems[actual].compareTo(elems[actual*2 + 2]) < 0));
    }
}
