package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        Fragment[] res = fragments.clone();
        //implemento insertion sort
        for(int i = 1; i < res.length; i++) {
            int j = i;
            while (j > 0 && res[j].compareTo(res[j-1]) < 0 ) {
                Fragment temp = res[j];
                res[j] = res[j-1];
                res[j-1] = temp;
                j--;
            }
        }
        return res;
    }

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        
        Router[] temp = new Router[k];
        Heapsort<Router> heap = new Heapsort(routers);
        
        int i = 0;
        while(i < temp.length && heap.maximo().getTrafico() > umbral) {
            temp[i] = heap.desencolar();
            i++;
        }

        Router[] res = new Router[i];

        i=0;
        while(i<res.length) {
            res[i] = temp[i];
            i++;
        }
        
        return res;
    }

    public IPv4Address[] sortIPv4(String[] ipv4) {

        //Genero una lista enlazada con los addresses
        ListaEnlazada<IPv4Address> lista = new ListaEnlazada<IPv4Address>();
        for (int i = 0; i < ipv4.length; i++) {
            IPv4Address address = new IPv4Address(ipv4[i]);
            lista.agregarAtras(address);
        }

        //Hago bucket sort 4 veces
        for (int i=3; i>=0; i--) {
            lista = bucketSort(lista, i);
        }

        //Convierto la lista enlazada ordenada a array.
        IPv4Address[] res = new IPv4Address[lista.longitud()];
        Iterador<IPv4Address> iterador = lista.iterador();
        int i = 0;
        while(iterador.haySiguiente()){
            res[i] = iterador.siguiente();
            i++;
        }
        
        return res;
    }

    private ListaEnlazada<IPv4Address> bucketSort(ListaEnlazada<IPv4Address> lista, int posicionOcteto) {
        ListaEnlazada<IPv4Address>[] buckets = new ListaEnlazada[256];
        Iterador<IPv4Address> iterador = lista.iterador();
        
        //agrega todos los addresses a sus buckets correspondientes
        while (iterador.haySiguiente()) {
            IPv4Address address = iterador.siguiente();
            int octeto = address.getOctet(posicionOcteto);
            if (buckets[octeto] == null) {
                buckets[octeto] = new ListaEnlazada<IPv4Address>();
                buckets[octeto].agregarAtras(address);
            } else {
                buckets[octeto].agregarAtras(address);
            }
            
        }

        //enlaza las listas
        ListaEnlazada<IPv4Address> res = new ListaEnlazada<IPv4Address>();
        for (int i=0; i < 256; i++) {
            if (buckets[i] != null) {
                res.encadenarConAliasing(buckets[i]);
            }    
        }

        return res;
    }

}
