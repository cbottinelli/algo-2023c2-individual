package aed;

import java.security.InvalidParameterException;
import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    int size;
    Nodo root;

    private class Nodo {
        Nodo p, i, d;
        T value;

        Nodo(T v) {
            p = null;
            i = null;
            d = null;
            value = v;
        }
    }

    public ABB() {
        size = 0;
        root = null;
    }

    public int cardinal() {
        return size;
    }

    public T minimo(){
        Nodo actual = root;
        while (actual.i != null) {
            actual = actual.i;
        }
        return actual.value;
    }

    public T maximo(){
        Nodo actual = root;
        while (actual.d != null) {
            actual = actual.d;
        }
        return actual.value;
    }

    public void insertar(T elem){

        Nodo nuevo = new Nodo(elem);

        if (root == null) {
            //si root == null, inserto el nuevo en root.
            
            root = nuevo;
            size++;

        } else {
            //si root != null, busco posicion e inserto nodo.
            
            Nodo actual = root;
            boolean insertado = false;
            while (!insertado) {
                int comparacion = elem.compareTo(actual.value);
                if (comparacion > 0) {
                    if (actual.d != null) {
                        actual = actual.d;
                    } else {
                        actual.d = nuevo;
                        nuevo.p = actual;
                        insertado = true;
                        size++;
                    }
                } else if (comparacion < 0) {
                    //No chequeo que no sea igual, por el requiere.
                    if (actual.i != null) {
                        actual = actual.i;
                    } else {
                        actual.i = nuevo;
                        nuevo.p = actual;
                        insertado = true;
                        size++;
                    }
                } else {
                    insertado = true;
                }
            }
        }
    }

    public boolean pertenece(T elem){
        
        boolean pertenece = false;
        Nodo actual = root;

        while (actual != null && !pertenece) {
            int comparacion = elem.compareTo(actual.value);
            if (comparacion == 0) {
                pertenece = true;
            } else if (comparacion > 0) {
                    actual = actual.d;
            } else {
                    actual = actual.i;
            }
        }

        return pertenece;
    }

    //hacer un esquema de como funciona la memoria dinamica en este metodo
    public void eliminar(T elem){
        Nodo e = buscar(root, elem);
        
        if (e == null) {
            //Caso 1: elem no esta en ABB
        } else if (e.d == null && e.i == null) {
            //Caso 2: e no tiene descendencia
            transplantar(e, null);
            size--;
        } else if (e.i == null) {
            //Caso 3a: e tiene solo el hijo derecho
            transplantar(e, e.d);
            size--;
        } else if (e.d == null) {
            //Caso 3b: e tiene solo el hijo izq
            transplantar(e, e.i);
            size--;
        } else {
            //Caso 4: e tiene dos hijos (Cormen lo hace mas breve)
            //busco el sucesor iterativamente

            Nodo sucesor = e.d;

            while (sucesor.i != null) {
                sucesor = sucesor.i;
            }

            //Como se que sucesor es caso 2 o caso 3, lo elimino con una llamada recursiva.
            eliminar(sucesor.value);
            
            //hago que e.p apunte a sucesor y que sucesor.p apunte a e.p
            transplantar(e, sucesor);
            
            //hago que sucesor apunte a los hijos de e y viceversa
            //chequeo si los hijos son nulos, porque quizas borre un hijo al borrar sucesor.
            if (e.i != null) {
                e.i.p = sucesor;
                sucesor.i = e.i;
            }
            if (e.d != null) {
                e.d.p = sucesor;
                sucesor.d = e.d;
            }
        }
    }

    public String toString(){

        String s = "{" + toString(root);
        s = s.substring(0, s.length()-1) + "}";

        return s;
    }

    private String toString(Nodo r) {
        String s = "";
        if (r != null) {
            s = toString(r.i) + r.value + "," + toString(r.d);
        }
        return s;
    }

    private Nodo buscar(Nodo actual, T elem) {
        //Busqueda recursiva de un nodo en ABB
        //Caso Base #1: elem no esta en ABB, con lo cual actual == null, para algun actual.
        //Caso Base #2: elem esta en ABB, con lo cual elem.compareTo(actual.value) == 0, para algun actual.
        //Caso recursivo: buscar en subarbol izq o subarbol derecho.
        Nodo b = null;
        
        if (actual != null) {
            int comparacion = elem.compareTo(actual.value);
            if (comparacion == 0) {
                b = actual;
            } else if (comparacion > 0) {
                b = buscar(actual.d, elem);
            } else {
                b = buscar(actual.i, elem);
            }
        }
        
        return b;

    }

    private void transplantar(Nodo e, Nodo t) {
        //La abstraccion surgio naturalmente, pero la puli con Cormen.
        //requiere: e pertenece a ABB e (inv. rep.).
        //Tener en cuenta: no elimina las referencias de los hijos a e, ni inicializa las referencias de los hijos de t.

        if (e.p == null) {
                root = t;
            } else if (e.p.d == e) {
                e.p.d = t;
            } else {
                e.p.i = t;
            }
        if (t != null) t.p = e.p;
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo actual;

        ABB_Iterador() {
            //posiciono actual en el minimo
            actual = minimo(root);
        }

        public boolean haySiguiente() { 
            return actual != null;
        }
    
        public T siguiente() {
            //requiere: actual != null
            T siguiente = actual.value;
            //muevo el puntero
            if (actual.d != null) {
                //el siguiente esta hacia abajo.
                actual = minimo(actual.d);
            } else {
                //actual es el maximo del arbol que tiene a actual por raiz.
                //busco hacia arriba el primer arbol que no tenga a actual por maximo.
                //si existe, la raiz de ese arbol es el siguiente.
                Nodo anterior = null;
                while (actual != null && (actual.i == null || actual.i != anterior)) {
                    anterior = actual;
                    actual = actual.p;
                }   
            }
            return siguiente;
        }
    }

    private Nodo minimo(Nodo raiz){
        //si la raiz es null, devuelve null
        Nodo minimo = raiz;
        if (minimo != null) {
            while (minimo.i != null) {
                minimo = minimo.i;
            }
        }
        return minimo;
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
